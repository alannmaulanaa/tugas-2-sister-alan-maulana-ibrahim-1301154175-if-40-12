#alan maulana ibrahim 1301154175
# import xmlrpc bagian client
import xmlrpc.client

# buat stub proxy client
proxy = xmlrpc.client.ServerProxy("http://192.168.1.194:6003")

# buka file yang akan diupload
with open("file_diupload.txt",'rb') as handle:
    # baca file dan ubah menjadi biner dengan xmlrpc.client.Binary
    data = xmlrpc.client.Binary(handle.read())
    handle.close()

# panggil fungsi untuk upload yang ada di server
result = proxy.file_diupload(data)
