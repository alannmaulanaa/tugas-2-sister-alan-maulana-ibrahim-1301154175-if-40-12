#ALAN MAULANA IBRAHIM 1301154175
# import xmlrpc bagian client saja
import xmlrpc.client

# buat stub (proxy) untuk client
proxy = xmlrpc.client.ServerProxy('http://127.0.0.1:8008')

# lakukan pemanggilan fungsi vote("nama_kandidat") yang ada di server
proxy.vote('JOKOWI'')
proxy.vote('JOKOWI'')
proxy.vote('JOKOWI'')
proxy.vote('PRABOWO'')
proxy.vote('PRABOWO'')

# lakukan pemanggilan fungsi querry() untuk mengetahui hasil persentase dari masing-masing kandidat
hasil = proxy.querry()

# lakukan pemanggilan fungsi lain terserah Anda
for i in range(0, len(hasil[0])):
	print(hasil[0][i], ':', hasil[1][i], ', ', hasil[2][i], '%')
