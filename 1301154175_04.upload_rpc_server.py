# Alan Maulana ibrahim 1301154175
# import SimpleXMLRPCServer bagian server
from xmlrpc.server import SimpleXMLRPCServer

# buat fungsi bernama file_upload()
def file_upload(filedata):
    
    # buka file 
    with open("hasil_upload.txt",'wb') as handle:
        #convert from byte to binary IMPORTANT!
        data1=filedata.data
        
        # tulis file tersebut
        handle.write(data1)
        handle.close()
        return True  #IMPORTANT
        
# must have return value
# else error messsage: "cannot marshal None unless allow_none is enabled"

# buat server
server = SimpleXMLRPCServer(('192.168.1.194', 6003))

# tulis pesan server telah berjalan
print ("running on port 6003")

# register fungsi 
server.register_function(file_upload, 'file_diupload')

# jalankan server
server.serve_forever()
