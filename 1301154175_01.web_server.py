# alan maulana ibrahim 1301154175
# import library socket karena akan digunakan request reply protocol sederhana
import socket

# definisikan IP dan port dari webserver yang akan kita gunakan. Port HTTP adalah 80
IP_Server = "192.168.1.12"
Port_Server = 80

# buat socket bertipe TCP
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# lakukan binding 
socket.bind((IP_Server,Port_Server))

# socket mendengarkan
socket.listen(5)

# tampilkan dengan print () "Server berjalan dan melayani HTTP pada port xx"
print("running http service on port ",Port_Server)

# loop forever
while True:
    # socket menerima koneksi
    client,client_address = socket.accept()
    
    # socket menerima data
    data = client.recv(1024).decode('utf-8')
    
    # print data hasil koneksi
    print(data)
    
    # buat response sesuai spesifikasi HTTP untuk diberikan kepada client
    http_response = """\HTTP/1.1 200 OK

<html>
<head>
<title>Web Server Sederhana</title>
</head>
<body>

<h1>Heading 1</h1>
<p>Ini adalah contoh paragraf.</p>
<img src="https://www.surfertoday.com/images/stories/surfetiquette.jpg">

</body>
</html>
"""
    # kirim response kepada client dengan sendall() jangan lupa diencode response dengan utf-8 
    client.sendall(http_response.encode(encoding='utf-8'))
    
    # tutup koneksi
    client.close()

# Selamat! Kamu telah berhasil membuat web server sederhana. 

