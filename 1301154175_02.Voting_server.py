# Alan Maulana ibrahim 1301154175
# import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCServer

# import SimpleXMLRPCRequestHandler
from xmlrpc.server import SimpleXMLRPCRequestHandler

import threading

# Batasi hanya pada path /RPC2 saja supaya tidak bisa mengakses path lainnya
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_path = ('/RPC2',)

# Buat server
with SimpleXMLRPCServer(('127.0.0.1', 8008), requestHandler=RequestHandler, allow_none=True) as server:
    server.register_introspection_functions()

    # buat data struktur dictionary untuk menampung nama_kandidat dan hasil voting
    vote = { "nama_kandidat" : [], 
             "hasil_voting" : []
            }
    
    # kode setelah ini adalah critical section, menambahkan vote tidak boeh terjadi race condition
    # siapkan lock
    lock = threading.Lock()
    
    #  buat fungsi bernama vote_candidate()
    def vote_candidate(x):
        
        # critical section dimulai harus dilock
        lock.acquire()
        # jika kandidat ada dalam dictionary maka tambahkan  nilai votenya
        if len(vote["nama_kandidat"]) is 0:
            vote["nama_kandidat"].append(x)
            vote["hasil_voting"].append(1)
        else:
            for a in range(0, len(vote["nama_kandidat"])):
                if vote["nama_kandidat"][a] == x:
                    vote["hasil_voting"][a] += 1
                    break
                if (a == len(vote["nama_kandidat"])-1):
                    vote["nama_kandidat"].append(x)
                    vote["hasil_voting"].append(1)
        
        # critical section berakhir, harus diunlock
        lock.release()
        return True
        
    
    # register fungsi vote_candidate() sebagai vote
    server.register_function(vote_candidate, 'vote')

    # buat fungsi bernama querry_result
    def querry_result():
        # critical section dimulai
        lock.acquire()
        
        # hitung total vote yang ada
        totalvote = sum(vote["hasil_voting"])
        
        # hitung hasil persentase masing-masing kandidat
        persentase = []
        for a in range(0, len(vote["nama_kandidat"])):
            hasil = vote["hasil_voting"][a]
            persentase.append((hasil/totalvote)*100)
        
        # critical section berakhir
        lock.release()    
        return[vote["nama_kandidat"], vote["hasil_voting"], persentase]
    # register querry_result sebagai querry
    server.register_function(querry_result, 'querry')


    print ("Running Votting.....")
    # Jalankan server
    server.serve_forever()
